package at.petritzdesigns.bitbot.simulation.view;

import at.petritzdesigns.bitbot.exceptions.BitBotServerException;
import at.petritzdesigns.bitbot.networking.BitBotServer;
import at.petritzdesigns.bitbot.networking.BitBotUdpServer;
import at.petritzdesigns.bitbot.simulation.control.SimulationRoboter;
import javax.swing.JOptionPane;

/**
 * BitBotSimulation
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class SimulationFrame extends javax.swing.JFrame {

    /**
     * Server that is used
     */
    private final BitBotServer server;

    /**
     * Simulation roboter
     */
    private final SimulationRoboter robot;

    /**
     * Default Constructor
     */
    public SimulationFrame() {
        initComponents();
        this.robot = new SimulationRoboter();
        this.server = new BitBotUdpServer(robot);
    }

    /**
     * Starts Server
     * @throws at.petritzdesigns.bitbot.exceptions.BitBotServerException
     */
    public void startServer() throws BitBotServerException {
        server.start();
    }

    /**
     * Stops server
     */
    public void stopServer() {
        server.stop();
    }
    
    /**
     * Whether server is running or not
     * @return running
     */
    public boolean isRunning() {
        return server.isRunning();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("BitBot Simulation");
        setMinimumSize(new java.awt.Dimension(800, 600));
        setPreferredSize(new java.awt.Dimension(800, 600));
        setSize(new java.awt.Dimension(800, 600));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                onClose(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 800, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 600, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Window closing event, to stop server before the application quits
     *
     * @param evt event
     */
    private void onClose(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_onClose
        if (server.isRunning()) {
            int ret = JOptionPane.showConfirmDialog(this,
                    "Der Server läuft noch. Möchstest du ihn stoppen und beenden?",
                    "Server läuft noch",
                    JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.WARNING_MESSAGE);

            switch (ret) {
                case JOptionPane.YES_OPTION:
                    server.stop();
                    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                    break;
                case JOptionPane.NO_OPTION:
                    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                    break;
                default:
                    setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
                    break;
            }
        }
    }//GEN-LAST:event_onClose

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
