package at.petritzdesigns.bitbot.simulation.control;

import at.petritzdesigns.bitbot.control.Roboter;
import at.petritzdesigns.bitbot.control.components.Motor;
import at.petritzdesigns.bitbot.exceptions.BitBotControlException;

/**
 * BitBotSimulation
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class SimulationRoboter implements Roboter {

    /**
     * Default Constructor
     */
    public SimulationRoboter() {
    }

    /**
     * Setups renderer and robot
     *
     * @return success
     * @throws BitBotControlException when something failed
     */
    @Override
    public boolean setup() throws BitBotControlException {
        //TODO
        return true;
    }

    /**
     * Shutdowns renderer and robot
     *
     * @return success
     * @throws BitBotControlException when something failed
     */
    @Override
    public boolean shutdown() throws BitBotControlException {
        //TODO
        return true;
    }

    @Override
    public double getRange() throws BitBotControlException {
        return 0;
    }

    @Override
    public Motor getLeftMotor() throws BitBotControlException {
        return null;
    }

    @Override
    public Motor getRightMotor() throws BitBotControlException {
        return null;
    }

}
