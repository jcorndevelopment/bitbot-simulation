package at.petritzdesigns.bitbot.simulation;

import at.petritzdesigns.bitbot.simulation.view.SimulationFrame;
import javax.swing.SwingUtilities;

/**
 * BitBotSimulation
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class BitBotSimulation {

    /**
     * Main entry point of the simulation
     *
     * @param args no arguments will be handled, it just starts the gui
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                SimulationFrame sf = new SimulationFrame();
                sf.setVisible(true);
            }
        });
    }

}
